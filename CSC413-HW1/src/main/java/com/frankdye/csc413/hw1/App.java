/*
Author: Frank Dye
ID: 912927332
Date: 1/30/2014
Description: Application driver class for Towers of Hanoi problem.
*/

package com.frankdye.csc413.hw1;


public class App 
{
    public static void main( String[] args )
    {
        System.out.println("Game 1: 3 Disks\n");
        
        Board hanoiBoard = new Board(3,3);
        Player hanoiPlayer = new Player(hanoiBoard);
        
        System.out.println();
        System.out.println("Game2: 10 Disks\n");
        Board hanoiBoard2 = new Board(3, 15);
        Player hanoiPlayer2 = new Player(hanoiBoard2);
    }
}
