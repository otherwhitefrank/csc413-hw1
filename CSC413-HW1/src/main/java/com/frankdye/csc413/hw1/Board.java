/*
 Author: Frank Dye
 ID: 912927332
 Date: 1/30/2014
 Description: The board class provides an interface for the Player class to make moves
 through. It also provides a continous check to see if we have solved the game.
 */
package com.frankdye.csc413.hw1;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Board
{

    private List<Peg> pegList;
    private int numPegs;
    private int numDisks;

    //Getter functions
    public int getNumPegs()
    {
        return numPegs;
    }

    public int getNumDisks()
    {
        return numDisks;
    }

    public Board(int inNumPegs, int inNumDisks)
    {
        //Create board with number of pegs and disks.
        pegList = new ArrayList();
        this.numPegs = inNumPegs;
        this.numDisks = inNumDisks;
        
        Peg tempPeg;
        for (int i = 0; i < inNumPegs; i++)
        {
            //Make pegs passing in pegID
            tempPeg = new Peg(i);
            pegList.add(tempPeg);
        }

        Disk tempDisk;

        //Acquire the first peg
        tempPeg = pegList.get(0);

        for (int i = inNumDisks; i > 0; i--)
        {
            //Make disks and add them to the first peg in our list
            tempDisk = new Disk("Disk " + i, i);
            tempPeg.addDisk(tempDisk);

        }

    }

    private void moveHelper(Peg inPeg1, Peg inPeg2) throws Peg.ExceptionPegEmpty
    {

        //This is a helper function to makeMove, makeMove handles the error checking and calls
        //this to perform the actual move and print output.
        Disk ptrDisk;

        ptrDisk = inPeg1.removeDisk();
        inPeg2.addDisk(ptrDisk);
        
        System.out.println("Moved: " + ptrDisk + " from " + inPeg1 + " to " + inPeg2);

    }

    public boolean makeMove(int peg1, int peg2)
    {
        Peg ptrPeg1, ptrPeg2;

        Disk ptrDisk1, ptrDisk2;

        //Acquire pegs, adjust for starting at index 0
        ptrPeg1 = pegList.get(peg1-1);
        ptrPeg2 = pegList.get(peg2-1);
        try
        {
            //Acquire disks from pegs
            if (!ptrPeg1.isEmpty() && !ptrPeg2.isEmpty())
            {

                //Each peg is not empty, so get top disks
                ptrDisk1 = ptrPeg1.peekDisk();
                ptrDisk2 = ptrPeg2.peekDisk();

                //Make sure target pegs disk is larger then source pegs disk.
                if (ptrDisk2.getWeight() > ptrDisk1.getWeight())
                {
                    //Weights are okay, make move
                    moveHelper(ptrPeg1, ptrPeg2);
                    
                    return true;
                }
                else
                {
                    //Weight of target peg is greater then source peg
                    //Can't make move
                    return false;
                }

            }
            else if (!ptrPeg1.isEmpty() && ptrPeg2.isEmpty())
            {
                //Second peg is empty, no reason to check weights of disks, just make the move
                moveHelper(ptrPeg1, ptrPeg2);

                return true;

            }
            else
            {
                //Both pegs empty, return false
                return false;
            }
        }
        catch (Peg.ExceptionPegEmpty ex)
        {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Shouldn't get here but if it does, fail
        return false;
    }

}
