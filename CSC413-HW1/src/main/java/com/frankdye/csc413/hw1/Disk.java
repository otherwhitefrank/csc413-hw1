/*
 Author: Frank Dye
 ID: 912927332
 Date: 1/30/2014
 Description: Disk class track of its weight, all other logic is maintained in the peg.
 */
package com.frankdye.csc413.hw1;

public class Disk
{

    private int weight;
    String name;

    //Default constructor
    public Disk(String name, int weight)
    {
        this.weight = weight;
        this.name = name;
    }
    
    
    //Getter / Setter functions
    
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }

    public int getWeight()
    {
        return weight;
    }

    //Pretty print 
    @Override
    public String toString()
    {
        return name + ": {weight=" + weight + '}';
    }
}
