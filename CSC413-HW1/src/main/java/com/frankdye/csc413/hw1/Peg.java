/*
Author: Frank Dye
ID: 912927332
Date: 1/30/2014
Description: A peg maintains a list of the disks that are currently residing on it,
             it also makes sure the top disk is the heaviest otherwise it generates an 
             exception.
*/

package com.frankdye.csc413.hw1;

import java.util.Stack;


public class Peg {
    private Stack<Disk> diskStack;
    private int pegID;

    //Default constructor takes a unique id for the Peg
    public Peg(int inPegID) {
        diskStack = new Stack();
        this.pegID = inPegID;
    }

    @Override
    public String toString() {
        return "Peg{" + "pegID=" + pegID + " , diskStack=" + diskStack + '}';
    }

    //Remove the top disk from the peg
    public Disk removeDisk() throws ExceptionPegEmpty {
        if (diskStack.isEmpty())
        {
            throw new ExceptionPegEmpty("No disks on peg\n" + this.toString());
        }
        else
        {
            return diskStack.pop();
        }
    }
    
    //Check to see pegs top disk without removing it
    public Disk peekDisk() throws ExceptionPegEmpty {
        if (diskStack.isEmpty())
        {
            throw new ExceptionPegEmpty("No disks on peg\n" + this.toString());
        }
        else
        {
            return diskStack.peek();
        }
            
    }
    
    //Check to see if peg is empty
    public boolean isEmpty() {
        return diskStack.isEmpty();
    }
    
    //Add a disk to peg
    public void addDisk(Disk inDisk) {
        diskStack.add(inDisk);
        
    }

    //Thrown if we try to pop an empty peg
    public static class ExceptionPegEmpty extends Exception {

        public ExceptionPegEmpty(String string) {
        }
    }
    
}
