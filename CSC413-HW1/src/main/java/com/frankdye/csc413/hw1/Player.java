/*
Author: Frank Dye
ID: 912927332
Date: 1/30/2014
Description: Player class that initiaties moves in the Towers of Hanoi problem, this class is somewhat abstract
             allowing each derived player to invoke a different strategy for solving the problem.
*/


package com.frankdye.csc413.hw1;


public class Player {

    Board currentBoard;
    Player(Board currentBoard)
    {
        //Default constructor
        this.currentBoard = currentBoard;
        
        int numberOfDisks = currentBoard.getNumDisks();
        this.playTowerHanoi(numberOfDisks, 1, 3);
    }
    
    void playTowerHanoi(int n, int sourcePeg, int destPeg)
    {
        /*Move the top n-1 disks from A to B
          Move the nth disk from A to C
	  Move the n-1 disks from B to C, using peg A as a utility
        */
        
        int helpPeg;
        
        //Assign helper peg
        if ((sourcePeg == 1 && destPeg == 3) || (sourcePeg == 3 && destPeg == 1))
        {
            helpPeg = 2;
        }
        else if ((sourcePeg == 1 && destPeg == 2) || (sourcePeg == 2 && destPeg == 1))
        {
            helpPeg = 3;
        }
        else
        {
            helpPeg = 1;
        }
        
        if (n == 1)
        {
            currentBoard.makeMove(sourcePeg, destPeg);
        }
        else
        {
            playTowerHanoi(n - 1, sourcePeg, helpPeg);
            currentBoard.makeMove(sourcePeg, destPeg);
            playTowerHanoi(n - 1, helpPeg, destPeg );
        }
        
    }
    
}
